package tippl.prosekhub.dto

class SubstDto(
	var name: String = "",
	var url: String = ""
)