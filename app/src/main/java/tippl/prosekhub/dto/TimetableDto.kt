package tippl.prosekhub.dto

class TimetableDto(
	var name: String = "",
	var pages: List<String> = emptyList(),
	var type: String = "",
	var url: String = "",
	var timestamp: Long = 0,
	var created: Long = 0
)
