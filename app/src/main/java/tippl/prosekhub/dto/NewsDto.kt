package tippl.prosekhub.dto

class NewsDto(
	var title: String = "",
	var description: String = "",
	var url: String = ""
)