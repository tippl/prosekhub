package tippl.prosekhub.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.crashlytics.android.Crashlytics
import java.util.*

class GeneratorCallbackReciever(private val callbacks: Queue<(Boolean) -> Unit>): BroadcastReceiver() {
	override fun onReceive(context: Context?, intent: Intent) {
		val result = intent.getBooleanExtra("success", true)
		Crashlytics.log(Log.INFO, "Generator", "Running callbacks.")
		while (!callbacks.isEmpty()) {
			callbacks.poll().invoke(result)
		}
	}
}