package tippl.prosekhub.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.view.View
import com.google.android.material.snackbar.Snackbar

class GeneratorMessageReceiver(private var view: View) : BroadcastReceiver() {
	override fun onReceive(context: Context, intent: Intent) {
		val message = intent.getStringExtra("message") ?: return
		Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
	}
}
