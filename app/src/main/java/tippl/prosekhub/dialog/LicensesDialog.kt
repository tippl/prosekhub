package tippl.prosekhub.dialog

import android.content.Context
import android.webkit.WebView
import androidx.appcompat.app.AlertDialog
import tippl.prosekhub.R

object LicensesDialog {

	fun createDialog(context: Context?) {
		context ?: return

		val dialog = AlertDialog.Builder(context, R.style.Theme_AppCompat_Light_Dialog_Alert)
			.setTitle(R.string.action_licenses)
			.setView(R.layout.dialog_licenses)
			.setPositiveButton(android.R.string.ok, null)
			.show()

		dialog.findViewById<WebView>(R.id.licenses_webview)!!
			.loadUrl("file:///android_asset/open_source/open_source_licenses.html")
	}
}