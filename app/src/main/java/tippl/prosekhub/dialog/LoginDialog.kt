package tippl.prosekhub.dialog

import android.content.Context
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.dd.processbutton.iml.ActionProcessButton
import com.google.firebase.auth.FirebaseAuth
import tippl.prosekhub.R

object LoginDialog {
	fun createDialog(context: Context?, callback: () -> Unit = {}) {
		context ?: return

		val dialog = AlertDialog.Builder(context, R.style.Theme_AppCompat_Dialog_Alert)
			.setView(R.layout.dialog_login)
			.setNegativeButton(android.R.string.cancel, null)
			.show()

		val password = dialog.findViewById<EditText>(R.id.password)!!
		val button = dialog.findViewById<ActionProcessButton>(R.id.login_button)!!

		button.setMode(ActionProcessButton.Mode.ENDLESS)
		button.setOnClickListener {
			button.progress = 1
			button.isEnabled = false

			val pass = password.text.toString()
			if (pass.isEmpty()) {
				button.progress = -1
				button.isEnabled = true
				return@setOnClickListener
			}

			FirebaseAuth.getInstance()
				.signInWithEmailAndPassword("app@prosekhub.local", pass)
				.addOnCompleteListener {
					if (it.isSuccessful) {
						dialog.dismiss()
						callback.invoke()
					} else {
						button.progress = -1
						button.isEnabled = true
					}
				}
		}
	}
}