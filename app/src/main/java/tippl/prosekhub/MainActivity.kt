package tippl.prosekhub

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.*
import tippl.prosekhub.enums.Theme
import tippl.prosekhub.observers.GeneratorBroadcastObserver
import tippl.prosekhub.util.FileUtil

class MainActivity : AppCompatActivity() {
	private val sp by lazy { PreferenceManager.getDefaultSharedPreferences(this) }

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setTheme(getThemeRes())
		setContentView(R.layout.activity_main)

		FileUtil.deleteOldSubst(this)
		FileUtil.deleteTmp(this)

		setSupportActionBar(toolbar)
		setupActionBarWithNavController(findNavController(R.id.main_nav_fragment))

		lifecycle.addObserver(GeneratorBroadcastObserver(this, coordinatorLayout))
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.main, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean =
		item.onNavDestinationSelected(findNavController(R.id.main_nav_fragment))

	override fun onSupportNavigateUp(): Boolean =
		findNavController(R.id.main_nav_fragment).navigateUp()

	private fun getThemeRes(): Int {
		val theme = sp.getString("app_theme", Theme.THEME_LIGHT.name)!!
		return Theme.valueOf(theme).res
	}
}
