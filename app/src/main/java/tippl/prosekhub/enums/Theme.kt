package tippl.prosekhub.enums

import tippl.prosekhub.R

enum class Theme(val res: Int) {
	THEME_LIGHT(R.style.AppThemeLight),
	THEME_DARK(R.style.AppThemeDark)
}