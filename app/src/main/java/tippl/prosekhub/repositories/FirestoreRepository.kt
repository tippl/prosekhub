package tippl.prosekhub.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import tippl.prosekhub.dto.NewsDto
import tippl.prosekhub.dto.SubstDto
import tippl.prosekhub.util.add
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

suspend fun loadFromFirestore(
	collection: String,
	orderBy: String = "timestamp",
	order: Query.Direction = Query.Direction.DESCENDING,
	limit: Long = 3,
	query: Query.() -> Query = { this }
) = suspendCoroutine<Task<QuerySnapshot>> { cont ->
	val db = FirebaseFirestore.getInstance()
	db.collection(collection).query().orderBy(orderBy, order).limit(limit).get().addOnCompleteListener {
		cont.resume(it)
	}
}

class FirestoreRepository {
	private var substLiveData: MutableLiveData<Resource<LinkedList<SubstDto>>> = MutableLiveData()
	private var newsLiveData: MutableLiveData<Resource<LinkedList<NewsDto>>> = MutableLiveData()

	private inline fun <reified T> MutableLiveData<Resource<LinkedList<T>>>.loadFromFirestore(
		collection: String,
		orderBy: String = "timestamp",
		order: Query.Direction = Query.Direction.DESCENDING,
		limit: Long = 3,
		prepend: Boolean = false
	) {
		val db = FirebaseFirestore.getInstance()
		value = Resource.loading(LinkedList())
		db.collection(collection).orderBy(orderBy, order).limit(limit).get().addOnCompleteListener { task ->
			value = if (task.isSuccessful && !task.result!!.isEmpty) {
				val list = LinkedList<T>()
				task.result!!.forEach { list.add(it.toObject(T::class.java), prepend) }
				Resource.success(list)
			} else {
				Resource.error(LinkedList())
			}
		}
	}

	fun loadSubst() = substLiveData.loadFromFirestore("substitutions", prepend = true)

	fun loadNews() = newsLiveData.loadFromFirestore("news")

	fun getSubst(): LiveData<Resource<LinkedList<SubstDto>>> = substLiveData

	fun getNews(): LiveData<Resource<LinkedList<NewsDto>>> = newsLiveData
}