package tippl.prosekhub.repositories

class Resource<T> private constructor(val status: Status, val data: T) {
	companion object {
		fun <T> success(data: T) = Resource(
			Status.SUCCESS,
			data
		)
		fun <T> loading(data: T) = Resource(
			Status.LOADING,
			data
		)
		fun <T> error(data: T) = Resource(
			Status.ERROR,
			data
		)
	}

	enum class Status {
		SUCCESS,
		LOADING,
		ERROR
	}
}