package tippl.prosekhub.viewmodels

import androidx.lifecycle.ViewModel
import tippl.prosekhub.repositories.FirestoreRepository

class MainFragmentViewModel: ViewModel() {
	private val repository = FirestoreRepository()

	init {
		repository.loadSubst()
		repository.loadNews()
	}

	fun loadSubst() = repository.loadSubst()

	fun loadNews() = repository.loadNews()

	fun getSubst() = repository.getSubst()

	fun getNews() = repository.getNews()
}