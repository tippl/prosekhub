package tippl.prosekhub.fragments

import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuInflater
import androidx.preference.CheckBoxPreference
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat
import com.google.firebase.auth.FirebaseAuth
import kotlinx.serialization.json.Json
import kotlinx.serialization.list
import kotlinx.serialization.serializer
import tippl.prosekhub.R
import tippl.prosekhub.dialog.LicensesDialog
import tippl.prosekhub.dialog.LoginDialog
import tippl.prosekhub.generating.Generator
import tippl.prosekhub.observers.AnalyticsObserver
import tippl.prosekhub.util.openWebsite

class SettingsFragment : PreferenceFragmentCompat() {
	private val generator by lazy { Generator(context!!) }
	private val auth by lazy { FirebaseAuth.getInstance() }
	private val sp by lazy { PreferenceManager.getDefaultSharedPreferences(context!!) }

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
		lifecycle.addObserver(AnalyticsObserver(this))
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
		menu.clear()


	override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
		addPreferencesFromResource(R.xml.settings)

		val selector = findPreference("isTeacher") as CheckBoxPreference
		val pageSelect = findPreference("class") as ListPreference
		val theme = findPreference("app_theme") as ListPreference

		pageSelect.setOnPreferenceChangeListener { p, o ->
			val preference = p as ListPreference
			preference.summary = preference.entries[preference.findIndexOfValue(o as String)]
			true
		}

		theme.summary = theme.entry
		theme.setOnPreferenceChangeListener { _, _ ->
			activity?.recreate()
			true
		}

		selector.setOnPreferenceChangeListener { preference, o ->
			setPageSelectTitles(o as Boolean)
			setPageSelectValues(o, true)

			generator.addCallback { preference.isEnabled = true }
			generator.addToQueue(Generator.Generable.SPLIT)
			selector.isEnabled = false
			true
		}

		if (auth.currentUser != null) {
			loggedIn()
		} else {
			findPreference("log_in").setOnPreferenceClickListener {
				LoginDialog.createDialog(context) { loggedIn() }
				true
			}
		}

		findPreference("licences").setOnPreferenceClickListener {
			LicensesDialog.createDialog(context)
			true
		}

		findPreference("source").setOnPreferenceClickListener {
			openWebsite(context!!, R.string.url_source)
			true
		}

		findPreference("privacy").setOnPreferenceClickListener {
			openWebsite(context!!, R.string.url_privacy_policy)
			true
		}

		setPageSelectTitles(selector.isChecked)
		setPageSelectValues(selector.isChecked, false)
	}



	private fun setListPref(pref: ListPreference, listName: String) {
		val list = Json.parse(String.serializer().list, sp.getString(listName, "[]")!!)
		val values = (1..list.size).map { it.toString() }

		pref.entries = list.toTypedArray()
		pref.entryValues = values.toTypedArray()
	}

	private fun setPageSelectTitles(isTeacher: Boolean) {
		val title = if (isTeacher) R.string.pref_teach_title else R.string.pref_class_title
		val dTitle = if (isTeacher) R.string.pref_teach_dTitle else R.string.pref_class_dTitle

		val pageSelect = findPreference("class") as ListPreference
		pageSelect.setTitle(title)
		pageSelect.setDialogTitle(dTitle)
	}

	private fun setPageSelectValues(isTeacher: Boolean, reset: Boolean) {
		val pageSelect = findPreference("class") as ListPreference
		setListPref(pageSelect, if (isTeacher) "teachList" else "classList")
		if (reset && pageSelect.entries.isNotEmpty()) {
			pageSelect.setValueIndex(0)
		}
		pageSelect.summary = pageSelect.entry
	}

	private fun loggedIn() {
		val login = findPreference("log_in")
		login.setSummary(R.string.logged_in_summary)
		login.isEnabled = false
	}
}
