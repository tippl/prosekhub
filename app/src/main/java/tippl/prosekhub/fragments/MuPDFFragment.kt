package tippl.prosekhub.fragments

import android.os.Bundle
import android.view.*
import android.view.ViewGroup.LayoutParams
import androidx.fragment.app.Fragment
import com.artifex.mupdf.viewer.MuPDFCore
import com.artifex.mupdf.viewer.PageAdapter
import com.artifex.mupdf.viewer.ReaderView
import com.crashlytics.android.Crashlytics
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_mupdf.*
import tippl.prosekhub.R
import tippl.prosekhub.observers.AnalyticsObserver

class MuPDFFragment : Fragment() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
		lifecycle.addObserver(AnalyticsObserver(this))
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
		menu.clear()

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
		inflater.inflate(R.layout.fragment_mupdf, container, false)


	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)

		val args = MuPDFFragmentArgs.fromBundle(arguments!!)
		val core: MuPDFCore

		try {
			core = MuPDFCore(args.path)
		} catch (e: Exception) {
			Crashlytics.logException(e)

			activity?.let {
				Snackbar.make(it.findViewById(R.id.coordinatorLayout),
					R.string.pdf_open_error,
					Snackbar.LENGTH_LONG).show()
			}

			fragmentManager?.popBackStack()
			return
		}

		val mDocView = ReaderView(activity)
		mDocView.adapter = PageAdapter(activity, core)
		mDocView.displayedViewIndex = args.page
		mupdf_wrapper.addView(
			mDocView,
			LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
		)
	}
}
