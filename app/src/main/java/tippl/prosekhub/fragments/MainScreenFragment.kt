package tippl.prosekhub.fragments

import android.os.Bundle
import android.preference.PreferenceManager
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.RelativeLayout
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.edit
import androidx.core.text.parseAsHtml
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_main_screen.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.list
import kotlinx.serialization.serializer
import tippl.prosekhub.R
import tippl.prosekhub.databinding.NewsItemBinding
import tippl.prosekhub.dialog.LoginDialog
import tippl.prosekhub.dto.SubstDto
import tippl.prosekhub.dto.TimetableDto
import tippl.prosekhub.generating.Generator
import tippl.prosekhub.observers.AnalyticsObserver
import tippl.prosekhub.repositories.Resource.Status
import tippl.prosekhub.repositories.loadFromFirestore
import tippl.prosekhub.util.*
import tippl.prosekhub.viewmodels.MainFragmentViewModel
import java.io.File
import java.util.*

class MainScreenFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
	private var subst: LinkedList<SubstDto> = LinkedList()
	private val sp by lazy { PreferenceManager.getDefaultSharedPreferences(context!!) }
	private val pdfManager: PdfManager by lazy { PdfManager(this, subst_progress) }
	private val auth by lazy { FirebaseAuth.getInstance() }
	private val viewModel by lazy {
		ViewModelProviders.of(this).get(MainFragmentViewModel::class.java)
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		lifecycle.addObserver(AnalyticsObserver(this))
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		viewModel.getSubst().observe(viewLifecycleOwner, Observer {
			fun updateSubst(progress: Boolean, click: Boolean) {
				subst_progress.visibility = if (progress) View.VISIBLE else View.INVISIBLE
				buttonSubst.isClickable = click
			}
			when (it.status) {
				Status.SUCCESS -> {
					subst = it.data
					updateSubst(progress = false, click = true)
				}
				Status.LOADING -> updateSubst(progress = true, click = false)
				Status.ERROR -> updateSubst(progress = false, click = false)
			}
		})

		viewModel.getNews().observe(viewLifecycleOwner, Observer { news ->
			when (news.status) {
				Status.SUCCESS -> {
					news_label.visibility = View.VISIBLE
					news_progress.visibility = View.INVISIBLE
					news_container.removeAllViews()
					news.data.forEach { item ->
						val binder = NewsItemBinding.inflate(inflater, news_container, false)
						item.description = item.description
							.parseAsHtml()
							.toString()
							.replace(" Číst více", "")
						binder.newsItem = item
						binder.root.setOnClickListener { openWebsite(activity!!, item.url) }
						news_container.addView(binder.root)
					}
				}
				Status.LOADING -> {
					news_progress.visibility = View.VISIBLE
					news_container.removeAllViews()
				}
				Status.ERROR -> news_progress.visibility = View.INVISIBLE
			}
		})

		return inflater.inflate(R.layout.fragment_main_screen, container, false)
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)

		swipe_container.setOnRefreshListener(this)
		swipe_container.setColorSchemeResources(
			R.color.holo_green_dark,
			R.color.holo_red_dark,
			R.color.holo_blue_dark,
			R.color.holo_orange_light
		)

		buttonClass.setOnClickListener { pdfManager.openPdf(PdfManager.TimetablePaths.CLASS) }
		buttonTeacher.setOnClickListener { pdfManager.openPdf(PdfManager.TimetablePaths.TEACHER) }
		buttonRoom.setOnClickListener { pdfManager.openPdf(PdfManager.TimetablePaths.ROOM) }
		buttonEven.setOnClickListener { pdfManager.openSplit(PdfManager.Week.EVEN) }
		buttonOdd.setOnClickListener { pdfManager.openSplit(PdfManager.Week.ODD) }

		val typedValue = TypedValue()
		activity!!.theme.resolveAttribute(R.attr.customButtonCurrentColor, typedValue, true)
		if (Calendar.getInstance().get(Calendar.WEEK_OF_YEAR) % 2 == 0)
			(buttonEven.parent as RelativeLayout).setBackgroundColor(typedValue.data)
		else
			(buttonOdd.parent as RelativeLayout).setBackgroundColor(typedValue.data)

		buttonSubst.setOnClickListener { view ->
			val menu = PopupMenu(activity!!, view)
			val names = ArrayList<String>()
			val online = NetworkUtil.isNetworkAvailable(context!!)

			for (item in subst) {
				val file = File(FileUtil.getPdfFilename(activity!!, item.url))
				menu.menu.add(item.name).isEnabled = URLUtil.isValidUrl(item.url) && online || file.exists()
				names.add(item.name)
			}
			menu.setOnMenuItemClickListener { pdfManager.openPdfLink(subst[names.indexOf(it.toString())].url) }
			menu.show()
		}
		buttonSubst.isClickable = false

		if (auth.currentUser != null) {
			loggedIn()
		} else {
			runOnce(context, "login") { LoginDialog.createDialog(context) { loggedIn() } }
		}
	}

	private fun updateTimetable() {
		val activity = activity ?: return

		fun checkTimetable(timetable: PdfManager.TimetablePaths) {
			val path = "${activity.applicationInfo.dataDir}/set/${timetable.filename}.pdf"

			GlobalScope.launch(Dispatchers.Main) {
				val task = loadFromFirestore(
					"timetables_protected",
					orderBy = "created",
					limit = 1
				) { whereEqualTo("type", timetable.toString()) }

				if (!task.isSuccessful || task.result!!.isEmpty)
					return@launch

				val document = task.result!!.documents[0]
				val dto = document.toObject(TimetableDto::class.java)!!
				val last = sp.getLong(timetable.lastUpdatePref, 0)

				if (last == dto.created)
					return@launch

				val result = downloadPdfAsync(dto.url, path).await()

				if (!result) {
					Snackbar.make(
						activity.findViewById(R.id.coordinatorLayout),
						R.string.pdf_download_failed,
						Snackbar.LENGTH_LONG
					).show()
					return@launch
				}

				sp.edit {
					putString(
						timetable.listName,
						Json.stringify(String.serializer().list, dto.pages)
					)
				}

				if (!sp.getBoolean("isTeacher", false)
					&& timetable === PdfManager.TimetablePaths.CLASS
					|| sp.getBoolean("isTeacher", false)
					&& timetable === PdfManager.TimetablePaths.TEACHER
				) {
					val generator = Generator(activity)
					generator.addCallback {
						if (it) {
							sp.edit { putLong(timetable.lastUpdatePref, dto.created) }
						}
					}
					generator.addToQueue(Generator.Generable.SPLIT)
				} else {
					sp.edit { putLong(timetable.lastUpdatePref, dto.created) }
				}
			}
		}

		val isWifiOnly = sp.getBoolean("autoCheck_wifiOnly", false)
		val isWifiActive = NetworkUtil.isActiveNetworkWifi(activity)
		if (isWifiOnly && !isWifiActive)
			return

		PdfManager.TimetablePaths.values().forEach(::checkTimetable)
	}

	private fun updateSubst() {
		subst_progress.visibility = View.VISIBLE
		buttonSubst.isClickable = false

		viewModel.loadSubst()
	}

	private fun updateNews() {
		news_progress.visibility = View.VISIBLE
		news_container.removeAllViews()

		viewModel.loadNews()
	}

	private fun showUi() {
		buttonClassParent.visibility = View.VISIBLE
		buttonTeacherParent.visibility = View.VISIBLE
		buttonRoomParent.visibility = View.VISIBLE
		buttonWeekParent.visibility = View.VISIBLE
	}

	private fun loggedIn() {
		showUi()
		updateTimetable()
	}

	override fun onRefresh() {
		updateSubst()
		updateNews()

		swipe_container.isRefreshing = false
	}
}