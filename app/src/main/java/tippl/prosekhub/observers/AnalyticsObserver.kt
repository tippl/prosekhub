package tippl.prosekhub.observers

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.firebase.analytics.FirebaseAnalytics

class AnalyticsObserver(private val fragment: Fragment) : LifecycleObserver {

	@OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
	fun onResume() {
		val activity = fragment.activity ?: return
		val className = fragment.javaClass.simpleName

		FirebaseAnalytics.getInstance(activity).setCurrentScreen(activity, className, className)
	}
}