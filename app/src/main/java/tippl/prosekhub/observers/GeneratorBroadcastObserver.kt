package tippl.prosekhub.observers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import tippl.prosekhub.generating.GeneratorService
import tippl.prosekhub.receivers.GeneratorMessageReceiver

class GeneratorBroadcastObserver(private val context: Context,
								 private val layout: View): LifecycleObserver {
	private val messageReceiver: BroadcastReceiver by lazy { GeneratorMessageReceiver(layout) }

	@OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
	fun onResume() {
		LocalBroadcastManager.getInstance(context)
			.registerReceiver(messageReceiver, IntentFilter(GeneratorService.GENERATOR_MESSAGE))
	}

	@OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
	fun onPause() {
		LocalBroadcastManager.getInstance(context)
			.unregisterReceiver(messageReceiver)
	}
}