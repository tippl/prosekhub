package tippl.prosekhub.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import androidx.core.content.getSystemService

object NetworkUtil {
	private fun getConnectivityManager(context: Context) =
		context.getSystemService<ConnectivityManager>()!!

	private fun getNetworkInfo(context: Context): NetworkInfo? =
		getConnectivityManager(context).activeNetworkInfo

	fun isNetworkAvailable(context: Context) =
		getNetworkInfo(context)?.isConnected ?: false

	@Suppress("DEPRECATION")
	fun isActiveNetworkWifi(context: Context): Boolean {
		return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			val connectivityManager = getConnectivityManager(context)
			val activeNetwork = connectivityManager.activeNetwork
			val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
			networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
		} else {
			getNetworkInfo(context)?.type == ConnectivityManager.TYPE_WIFI
		}
	}
}
