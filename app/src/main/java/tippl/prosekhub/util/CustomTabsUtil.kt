package tippl.prosekhub.util

import android.content.Context
import androidx.annotation.StringRes
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.res.ResourcesCompat
import androidx.core.net.toUri
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import tippl.prosekhub.R

fun openWebsite(context: Context, @StringRes id: Int) {
	openWebsite(context, context.getString(id))
}

fun openWebsite(context: Context, url: String) {
	CustomTabsIntent.Builder()
		.setToolbarColor(ResourcesCompat.getColor(context.resources, R.color.primary, null))
		.build()
		.launchUrl(context, url.toUri())
}
