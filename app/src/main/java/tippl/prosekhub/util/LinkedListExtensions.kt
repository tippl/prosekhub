package tippl.prosekhub.util

import java.util.*

fun <T> LinkedList<T>.add(item: T, prepend: Boolean) {
	if (prepend) {
		addFirst(item)
	} else {
		addLast(item)
	}
}