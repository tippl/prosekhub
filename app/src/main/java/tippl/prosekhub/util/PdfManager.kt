package tippl.prosekhub.util

import android.app.Activity
import android.app.AlertDialog
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.crashlytics.android.Crashlytics
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.list
import kotlinx.serialization.serializer
import tippl.prosekhub.R
import tippl.prosekhub.fragments.MainScreenFragmentDirections
import java.io.File

class PdfManager(fragment: Fragment, private val bar: ProgressBar) {

	private val activity: Activity = fragment.activity!!
	private val basePath = activity.applicationInfo.dataDir
	private val navController = fragment.findNavController()
	private val tag = "PdfManager"

	fun openPdf(timetable: TimetablePaths): Boolean {
		Crashlytics.log(Log.INFO, tag, "Open $timetable")
		val sp = PreferenceManager.getDefaultSharedPreferences(activity)
		val path = "$basePath/set/${timetable.filename}.pdf"
		val stringList = Json.parse(String.serializer().list, sp.getString(timetable.listName, "[]")!!)
		if (File(path).exists()) {
			if (stringList.isNotEmpty()) {
				listPopup(path, timetable.popupString, stringList)
			} else {
				open(path)
			}
		} else {
			Snackbar.make(
				activity.findViewById(R.id.coordinatorLayout),
				R.string.not_generated_pdf,
				Snackbar.LENGTH_LONG
			).show()
			return false
		}
		return true
	}

	fun openSplit(week: Week): Boolean {
		Crashlytics.log(Log.INFO, tag, "Open $week")
		val sp = PreferenceManager.getDefaultSharedPreferences(activity)
		val page = sp.getString("class", "1")

		val path = "$basePath/spl/$page-${week.id}.pdf"
		val file = File(path)
		if (file.exists()) {
			open(path)
		} else {
			Snackbar.make(
				activity.findViewById(R.id.coordinatorLayout),
				R.string.not_generated_pdf,
				Snackbar.LENGTH_LONG
			).show()
			return false
		}
		return true
	}

	fun openPdfLink(urlPdf: String): Boolean {
		Crashlytics.log(Log.INFO, tag, "Open $urlPdf")
		val to = FileUtil.getPdfFilename(activity, urlPdf)

		when {
			NetworkUtil.isNetworkAvailable(activity) -> {
				bar.visibility = View.VISIBLE
				GlobalScope.launch(Dispatchers.Main) {
					val result = downloadPdfAsync(urlPdf, to).await()

					if (result) {
						open(to)
					} else {
						Snackbar.make(
							activity.findViewById(R.id.coordinatorLayout),
							R.string.pdf_download_failed,
							Snackbar.LENGTH_LONG
						).show()
					}
					bar.visibility = View.INVISIBLE
				}
			}
			File(to).exists() -> open(to)
			else -> {
				Snackbar.make(
					activity.findViewById(R.id.coordinatorLayout),
					R.string.no_internet,
					Snackbar.LENGTH_LONG
				).show()
				return false
			}
		}
		return true
	}

	private fun open(path: String, page: Int = 0) {
		if (navController.currentDestination?.id == R.id.mainScreenFragment) {
			val directions = MainScreenFragmentDirections.actionMainScreenFragmentToMuPDFFragment(path)
			directions.page = page
			navController.navigate(directions)
		} else {
			Crashlytics.log(Log.INFO, "Navigation", "Attempted navigation to pdf viewer, however current location was not MainScreenFragment. Aborting ($path)")
		}
	}

	private fun listPopup(path: String, @StringRes resId: Int, list: List<String>) {
		listPopup(path, activity.getString(resId), list)
	}

	private fun listPopup(path: String, head: String, list: List<String>) {
		val builder = AlertDialog.Builder(activity)
		builder.setTitle(head)
		builder.setItems(list.toTypedArray()) { _, which -> open(path, which) }
		builder.show()
	}

	enum class TimetablePaths(
		val filename: String,
		val listName: String,
		val lastUpdatePref: String,
		val popupString: Int
	) {
		CLASS("rozvrh", "classList", "lastClass", R.string.select_class),
		TEACHER("ucitele", "teachList", "lastTeach", R.string.select_teacher),
		ROOM("mistnosti", "roomList", "lastRoom", R.string.select_room)
	}

	enum class Week(val id: String) {
		EVEN("1"),
		ODD("2")
	}
}
