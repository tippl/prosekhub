package tippl.prosekhub.util

import android.content.Context
import android.preference.PreferenceManager
import androidx.core.content.edit

fun runOnce(context: Context?, name: String, execute: () -> Unit) {
	context ?: return

	val sp = PreferenceManager.getDefaultSharedPreferences(context)
	val executed = sp.getBoolean("once_$name", false)

	if (!executed) {
		execute.invoke()
		sp.edit { putBoolean("once_$name", true) }
	}
}