package tippl.prosekhub.util

import android.content.Context

import org.apache.commons.io.FilenameUtils

import java.io.File
import java.io.FileFilter

object FileUtil {
	private const val WEEK_MILLIS = 1000 * 60 * 60 * 24 * 7

	fun getPdfFilename(context: Context, url: String) =
		"${context.applicationInfo.dataDir}/pdf/${FilenameUtils.getBaseName(url)}.pdf"


	fun deleteRecursive(fileOrDirectory: File) {
		if (fileOrDirectory.isDirectory) {
			fileOrDirectory.listFiles().forEach(::deleteRecursive)
		}

		fileOrDirectory.delete()
	}

	fun deleteOldSubst(context: Context) {
		val file = File("${context.applicationInfo.dataDir}/pdf")
		val weekAgo = System.currentTimeMillis() - WEEK_MILLIS
		if (file.exists() && file.isDirectory) {
			file.listFiles(FileFilter { it.lastModified() < weekAgo }).forEach { it.delete() }
		}
	}

	fun deleteTmp(context: Context) {
		val file = File("${context.applicationInfo.dataDir}/tmp")
		deleteRecursive(file)
	}
}
