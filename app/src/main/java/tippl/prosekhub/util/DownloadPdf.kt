package tippl.prosekhub.util

import android.util.Log

import com.crashlytics.android.Crashlytics
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

import org.apache.commons.io.FileUtils

import java.io.File
import java.net.URL

fun downloadPdfAsync(from: String, to: String) = GlobalScope.async {
	val url = URL(from)
	val tmp = File.createTempFile("pdf-", ".tmp")
	val toFile = File(to)
	val dir = toFile.parentFile

	if (!dir.exists()) {
		dir.mkdirs()
	}

	Crashlytics.log(Log.INFO, "Downloading", "Start $from -> $to")

	try {
		FileUtils.copyURLToFile(url, tmp)
		tmp.renameTo(toFile)
		Crashlytics.log(Log.INFO, "Downloading", "Finished $to")
		true
	} catch (e: Exception) {
		e.printStackTrace()
		Crashlytics.logException(e)
		false
	}
}