package tippl.prosekhub.generating

import android.content.Context
import android.preference.PreferenceManager

import com.crashlytics.android.Crashlytics
import com.tom_roush.pdfbox.pdmodel.PDDocument
import com.tom_roush.pdfbox.pdmodel.common.PDRectangle

import java.io.File

import tippl.prosekhub.util.FileUtil
import tippl.prosekhub.util.PdfManager

internal class SplitGenerator(private val context: Context) : GenerateItem {
	private val basePath = context.applicationInfo.dataDir

	override fun generate(): Boolean {
		val sp = PreferenceManager.getDefaultSharedPreferences(context)
		val isTeacher = sp.getBoolean("isTeacher", false)

		val timetable =
			if (isTeacher) PdfManager.TimetablePaths.TEACHER else PdfManager.TimetablePaths.CLASS
		val file = File("$basePath/set/${timetable.filename}.pdf")

		if (!file.exists()) {
			return false
		}

		val dir = File("$basePath/spl")
		if (dir.exists()) {
			FileUtil.deleteRecursive(dir)
		}

		if (!dir.exists()) {
			dir.mkdir()
		}

		val pdDoc: PDDocument
		try {
			pdDoc = PDDocument.load(file)
		} catch (e: Exception) {
			Crashlytics.logException(e)
			return false
		}
		val pages = pdDoc.documentCatalog.pages
		var cou = 1
		for (page in pages) {
			for (i in 1..2) {
				val rect = PDRectangle()
				if (i == 1) {
					rect.upperRightY = 842f
					rect.lowerLeftY = 421f
				} else {
					rect.upperRightY = 421f
					rect.lowerLeftY = 0f
				}
				rect.upperRightX = page.cropBox.upperRightX
				rect.lowerLeftX = page.cropBox.lowerLeftX
				page.cropBox = rect
				val document = PDDocument()
				document.addPage(page)
				document.save("$basePath/spl/$cou-$i.pdf")
				document.close()
			}
			cou++
		}
		pdDoc.close()
		return true
	}

}

