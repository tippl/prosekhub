package tippl.prosekhub.generating

interface GenerateItem {
	fun generate(): Boolean
}
