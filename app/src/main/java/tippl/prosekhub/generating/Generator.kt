package tippl.prosekhub.generating

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.annotation.StringRes
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import tippl.prosekhub.R
import tippl.prosekhub.receivers.GeneratorCallbackReciever
import java.util.*
import kotlin.reflect.KClass

class Generator(private val context: Context) {
	private val callbacks: Queue<(Boolean) -> Unit> by lazy { LinkedList<(Boolean) -> Unit>() }

	init {
		LocalBroadcastManager.getInstance(context)
			.registerReceiver(GeneratorCallbackReciever(callbacks), IntentFilter(GeneratorService.GENERATOR_FINISHED))
	}

	private fun runGenerator(item: Generable) {
		val intent = Intent(context, GeneratorService::class.java)
		intent.putExtra("to_generate", item)
		context.startService(intent)
	}

	fun addToQueue(vararg items: Generable) {
		for (item in items) {
			runGenerator(item)
		}
	}

	fun addCallback(callback: (Boolean) -> Unit) =
		callbacks.add(callback)

	enum class Generable(val generator: KClass<out GenerateItem>, @StringRes val startMessage: Int) {
		SPLIT(SplitGenerator::class, R.string.generating_split)
	}
}
