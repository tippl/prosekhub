package tippl.prosekhub.generating

import android.app.IntentService
import android.content.Context
import android.content.Intent
import androidx.annotation.StringRes
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.util.Log

import com.crashlytics.android.Crashlytics

import tippl.prosekhub.R
import kotlin.reflect.full.primaryConstructor

class GeneratorService : IntentService("GeneratorService") {
	private var cancelled = false

	override fun onHandleIntent(intent: Intent) {
		if (cancelled)
			return

		val type = intent.getSerializableExtra("to_generate") as Generator.Generable
		val generateItemClass = type.generator
		val gen = generateItemClass.primaryConstructor!!.call(this)

		Crashlytics.log(Log.INFO, "Generator", "Generating: " + type.toString())
		sendMessage(this, type.startMessage)

		if ((!gen.generate()))
			cancelled = true
	}

	override fun onDestroy() {
		sendMessage(
			this,
			if (cancelled) R.string.generation_failed else R.string.generating_finished
		)
		LocalBroadcastManager.getInstance(this)
			.sendBroadcast(Intent(GENERATOR_FINISHED).putExtra("success", !cancelled))

		val result = if (cancelled) "failed" else "finished"
		Crashlytics.log(Log.INFO, "Generator", "Generating $result")

		super.onDestroy()
	}

	companion object {
		const val GENERATOR_MESSAGE = "prosekhub.GENERATOR_MESSAGE"
		const val GENERATOR_FINISHED = "prosekhub.GENERATOR_FINISHED"

		fun sendMessage(context: Context, message: String) {
			val intent = Intent(GENERATOR_MESSAGE)
			intent.putExtra("message", message)
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
		}

		fun sendMessage(context: Context, @StringRes res: Int) {
			sendMessage(context, context.getString(res))
		}
	}
}
